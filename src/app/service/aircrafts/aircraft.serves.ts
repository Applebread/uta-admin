import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";

export interface IAircraftTypes {
    totalResults: number,
    items: Array<IAircraftType>
}

export interface IAircraftList {
    totalResults: number,
    items: Array<IAircraft>
}

export interface IAircraftType {
    "id"?: Number,
    "icao": string,
    "name": string,
    "description": string,
    "image": string,
    "heavy": boolean
}

export interface IAircraft {
    "id"?: number,
    "icao": string
    "location": string
    "gate": string
    "registration": string
    "fob": number
    stats?: IAircraftStats
}

export interface IAircraftStats {
    lastFlightAt: string;
    acTotalFH: string;
    acTotalFC: number;
    latestFiftyCheckAt: string;
    latestTwoHundredCheckAt: string;
    latestThousandCheckAt: string;
}

export interface AircraftImageUploadStatus {
    status: string;
    filename: string;
}

@Injectable({
    providedIn: 'root'
})
export class AircraftService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public aircraftTypes(page: number = 1, pageSize: number = 15): Observable<IAircraftTypes> {
        return this.http.get<IAircraftTypes>(this.config.apiEndpoint + "/api/admin/aircraft-types", {withCredentials: true});
    }

    public aircraftList(page: number = 1, pageSize: number = 15): Observable<IAircraftList> {
        return this.http.get<IAircraftList>(this.config.apiEndpoint + "/api/admin/aircrafts", {withCredentials: true});
    }

    public aircraft(id: Number): Observable<IAircraft> {
        return this.http.get<IAircraft>(this.config.apiEndpoint + "/api/admin/aircraft/" + id, {withCredentials: true});
    }

    public createAircraft(aircraft: IAircraft): Observable<IAircraft> {
        return this.http.post<IAircraft>(this.config.apiEndpoint + "/api/admin/aircraft", aircraft, {withCredentials: true});
    }

    public updateAircraft(id: number, aircraft: IAircraft): Observable<IAircraft> {
        return this.http.put<IAircraft>(this.config.apiEndpoint + "/api/admin/aircraft/" + id, aircraft, {withCredentials: true});
    }

    public aircraftType(id: Number): Observable<IAircraftType> {
        return this.http.get<IAircraftType>(this.config.apiEndpoint + "/api/admin/aircraft/type/" + id, {withCredentials: true});
    }

    public updateAircraftType(id: Number, aircraftType: IAircraftType): Observable<IAircraftType> {
        return this.http.put<IAircraftType>(this.config.apiEndpoint + "/api/admin/aircraft/type/" + id, aircraftType, {withCredentials: true});
    }

    public uploadAircraftTypeImage(file: File): Observable<AircraftImageUploadStatus> {
        const formData = new FormData();
        formData.append('aircraft_type_image', file, file.name);

        return this.http.post<AircraftImageUploadStatus>(this.config.apiEndpoint + "/api/admin/aircraft/type/image", formData, {withCredentials: true})
    }

    public createAircraftType(aircraftType: IAircraftType): Observable<IAircraftType> {
        return this.http.post<IAircraftType>(this.config.apiEndpoint + "/api/admin/aircraft/type", aircraftType, {withCredentials: true});
    }
}
