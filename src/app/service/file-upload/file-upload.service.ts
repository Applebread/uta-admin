import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {AiracStatusDto} from "../../dto/airac-status/airac-status.dto";
import {UploadStatusDto} from "../../dto/airac-status/upload-status.dto";

@Injectable({
    providedIn: 'root'
})
export class FileUploadService {
    static STATUSES_TO_RECHECK = ['progress', 'starting'];

    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

    public upload(file: File, formInputName: string = "file"): Observable<UploadStatusDto> {
        const formData = new FormData();
        formData.append(formInputName, file, file.name);

        return this.http.post<UploadStatusDto>(this.config.apiEndpoint + "/api/admin/navigation/upload-airac", formData, {withCredentials: true});
    }

    public checkStatusProgress(airacId?: number): Observable<AiracStatusDto> {
        return this.http.get<AiracStatusDto>(this.config.apiEndpoint + "/api/admin/navigation/airac/progress/" + airacId, {withCredentials: true})
    }

    public checkCurrentAiracStatus(): Observable<AiracStatusDto> {
        return this.http.get<AiracStatusDto>(this.config.apiEndpoint + "/api/admin/navigation/airac/current", {withCredentials: true})
    }
}
