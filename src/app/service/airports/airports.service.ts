import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";

export interface IAirportsList {
    totalResults: number;
    items: Array<IAirport>;
}

export interface IAirport {
    id?: number;
    name: string;
    icao: string;
    latitude: string;
    longitude: string;
    city: string;
    timezone: string;
    source: string;
}

@Injectable({
    providedIn: 'root'
})
export class AirportsService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public airportList(page: number = 1, pageSize: number = 15): Observable<IAirportsList>
    {
        const params = new HttpParams().set('page', page).set('page_size', pageSize);

        return this.http.get<IAirportsList>(this.config.apiEndpoint + "/api/airports/list", {params, withCredentials: true});
    }

    public airport(id: number):Observable<IAirport>
    {
        return this.http.get<IAirport>(`${this.config.apiEndpoint}/api/airport/${id}`, {withCredentials: true});
    }

    public updateAirport(id: number, airport: IAirport):Observable<IAirport>
    {
        return this.http.put<IAirport>(`${this.config.apiEndpoint}/api/airport/${id}`, airport, {withCredentials: true});
    }

    public createAirport(airport: IAirport):Observable<IAirport>
    {
        return this.http.post<IAirport>(`${this.config.apiEndpoint}/api/airport`, airport, {withCredentials: true});
    }
}
