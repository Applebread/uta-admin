import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";

export interface IPilotsList {
    totalResults: number,
    items: Array<IPilot>
}

export interface IPilot {
    id: Number
    pid: string
    email: string
    name: string
    flightTime: string
    avatar: string
    minimum: string
    rank: string
    rankIcon: string
    logbookItems: Array<ILogbookItem>
}

interface ILogbookItem {
    created: string
    from: string
    to: string
    flightTime: string
    landingAirport: string
    route: string
}

export interface IRank {
    id: Number
    name: string
    icon: string
}

export interface IMinimum {
    id: Number
    value: string
}

@Injectable({
    providedIn: 'root'
})
export class PilotsService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public ranksList(): Observable<Array<IRank>> {
        return this.http.get<Array<IRank>>(this.config.apiEndpoint + "/api/rank/list", {withCredentials: true});
    }

    public minimumsList(): Observable<Array<IMinimum>> {
        return this.http.get<Array<IMinimum>>(this.config.apiEndpoint + "/api/minimum/list", {withCredentials: true});
    }

    public pilotsList(page: number = 1, pageSize: number = 15): Observable<IPilotsList>
    {
        const params = new HttpParams().set('page', page).set('page_size', pageSize);

        return this.http.get<IPilotsList>(this.config.apiEndpoint + "/api/pilots/list", {params, withCredentials: true});
    }

    public updatePilotMinimum(pilot: IPilot, minimum: IMinimum): Observable<any>
    {
        return this.http.put(this.config.apiEndpoint + `/api/pilot/${pilot.id}/minimum/${minimum.id}`, {}, {withCredentials: true})
    }

    public updatePilotRank(pilot: IPilot, rank: IRank): Observable<any>
    {
        return this.http.put(this.config.apiEndpoint + `/api/pilot/${pilot.id}/rank/${rank.id}`, {}, {withCredentials: true})
    }
}
