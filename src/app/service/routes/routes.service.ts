import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";

export interface IRoute {
    id: Number
    flightNumber: string
    from: string
    to: string
    daysOfWeek: Array<Number>
    departureTime: string
    scheduledSince: string
    scheduledTill: string
    aircraftType: string
    alternates: Array<string>
}

export interface IRouteList {
    totalResults: number,
    items: Array<IRoute>
}

export interface IRouteFilter {
    aircraftType?: string
    dateOfFlight?: string
    from?: string
    to?: string
}

@Injectable({
    providedIn: 'root'
})
export class RoutesService {

    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    public routesList(page: number = 1, pageSize: number = 15, filter?: IRouteFilter): Observable<IRouteList>
    {
        const params = new HttpParams().set('page', page).set('page_size', pageSize);

        return this.http.post<IRouteList>(this.config.apiEndpoint + "/api/flight/timetable", filter, {params, withCredentials: true});
    }

    public route(id: number): Observable<IRoute>
    {
        return this.http.get<IRoute>(this.config.apiEndpoint + "/api/flight/" + id,{withCredentials: true});
    }
}
