import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";

export interface IQuestionBankItem {
    id?: number,
    type: string,
    question: string,
    options: Array<IQuestionBankItemOption>
    aircraftType?: string
}

export interface IQuestionBankItemOption {
    "id": number,
    "text": string,
    "isCorrect": boolean
}

export interface IQuestionList {
    totalResults: number,
    items: Array<IQuestionBankItem>
}

export interface IQuestionExistentType {
    type: string,
    aircraftType: string | null
}

@Injectable({
    providedIn: 'root'
})
export class QuestionBankService {
    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    }

    questionTypes():Observable<Array<string>> {
        return this.http.get<Array<string>>(this.config.apiEndpoint + "/api/admin/exam/question-bank/types", {withCredentials: true})
    }

    questionExistentTypes(): Observable<Array<IQuestionExistentType>> {
        return this.http.get<Array<IQuestionExistentType>>(this.config.apiEndpoint + "/api/admin/exam/question-bank/existent-types", {withCredentials: true})
    }

    public questionsList(type: string, acType: string, page: number = 1, pageSize: number = 15): Observable<IQuestionList>
    {
        const params = new HttpParams()
            .append('type', type)
            .append('ac_type', acType)
            .append('page', page)
            .append('page_size', pageSize);

        return this.http.get<IQuestionList>(this.config.apiEndpoint + "/api/admin/exam/question-bank", {params, withCredentials: true});
    }

    public question(id: number): Observable<IQuestionBankItem> {
        return this.http.get<IQuestionBankItem>(this.config.apiEndpoint + "/api/admin/exam/question-bank/" + id, {withCredentials: true})
    }

    public createQuestion(newItem: IQuestionBankItem): Observable<IQuestionBankItem>
    {
        return this.http.post<IQuestionBankItem>(this.config.apiEndpoint + "/api/admin/exam/question-bank", newItem, {withCredentials: true});
    }

    public updateQuestion(id: number, item: IQuestionBankItem): Observable<IQuestionBankItem>
    {
        return this.http.put<IQuestionBankItem>(this.config.apiEndpoint + "/api/admin/exam/question-bank/" + id, item, {withCredentials: true});
    }
}
