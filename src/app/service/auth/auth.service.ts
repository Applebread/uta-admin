import {Inject, Injectable} from '@angular/core';

import {HttpClient, HttpResponse} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

interface ILoggedInUser {
    pid: string,
    email: string,
    roles: Array<string>
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig, private router: Router, private snackBar: MatSnackBar) {
        this.isUserLoggedIn = !!localStorage.getItem('loggedInUser') && !!localStorage.getItem('token');

        const storedUser: string | null = localStorage.getItem('loggedInUser');
        this.loggedInUser = storedUser ? JSON.parse(storedUser) : undefined;
    }

    isUserLoggedIn: boolean = false;
    loggedInUser?: ILoggedInUser;

    async login(username: string, password: string): Promise<void> {
        await this.http.post<any>(this.config.apiEndpoint + '/api/auth', {username, password}, {
            observe: 'response',
            withCredentials: true
        }).subscribe(async (user: any) => {
            localStorage.setItem('token', user.body.token);

            await this.http.get<ILoggedInUser>(this.config.apiEndpoint + '/api/auth/info', {
                observe: 'response',
                withCredentials: true,
                headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
            }).subscribe(async (user: HttpResponse<ILoggedInUser>) => {
                this.loggedInUser = user.body ? user.body : undefined;
                localStorage.setItem('loggedInUser', JSON.stringify(this.loggedInUser));

                if (!this.loggedInUser?.roles.includes('ROLE_ADMIN')) {
                    this.snackBar.open('Not permitted', 'close');

                    await this.logout();

                    return;
                }

                await this.router.navigate(['/', 'dashboard']);
            });
        }, (err: any) => {
            this.loggedInUser = undefined;

            localStorage.removeItem('token');
            localStorage.removeItem('loggedInUser');

            this.snackBar.open(err, 'close');
        })
    }

    async logout(): Promise<void> {
        this.loggedInUser = undefined;
        localStorage.removeItem('token');
        localStorage.removeItem('loggedInUser');

        await this.router.navigate(['/', 'login']);
    }
}
