import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FileUploadService} from "../../../service/file-upload/file-upload.service";
import {HttpClient} from "@angular/common/http";
import {AiracStatusDto} from "../../../dto/airac-status/airac-status.dto";
import {UploadStatusDto} from "../../../dto/airac-status/upload-status.dto";

interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}

@Component({
    selector: "app-airac-upload",
    templateUrl: "./airac-upload.component.html",
    styleUrls: ["./airac-upload.component.scss"]
})

export class AiracUploadComponent implements OnInit {
    @ViewChild('fileInput') fileInput: ElementRef<HTMLInputElement> | null = null;

    public currentCycle?: string;

    public file?: File;

    public activeFileName?: string;

    public activeAiracId?: number;

    public activeStatus?: string;

    public loading: boolean = false;

    private recheckInterval?: any;

    private fileUploadService: FileUploadService;

    private http: HttpClient;

    constructor(fileUploadService: FileUploadService, http: HttpClient) {
        this.fileUploadService = fileUploadService;
        this.http = http;
    }

    onChange(event: HTMLInputEvent): void {
        if (!event.target || !event.target.files) {
            return;
        }

        this.file = event.target.files[0];
    }

    recheckFileStatus(): void {
        this.loading = true;
        this.recheckInterval = setInterval(() => {
            if (typeof this.activeStatus === 'undefined') {
                return;
            }

            this.loading = true;
            if (FileUploadService.STATUSES_TO_RECHECK.indexOf(this.activeStatus) === -1) {
                this.stopRechecking();
                this.loading = false;
            }

            this.fileUploadService.checkStatusProgress(this.activeAiracId).subscribe((data: AiracStatusDto) => {
                this.activeFileName = data.file_name
                this.activeStatus = data.status
                this.currentCycle = data.cycle || undefined
            })
        }, 5000)
    }

    stopRechecking(): void {
        clearInterval(this.recheckInterval);
    }

    onUpload(): void {
        if (!this.file) {
            return;
        }

        this.loading = true;

        this.fileUploadService.upload(this.file, "airac_file").subscribe(
            (event: UploadStatusDto) => {
                this.activeFileName = this.file?.name;
                this.activeAiracId = event.id;
                this.activeStatus = 'starting';

                if (typeof (event) === 'object') {
                    this.recheckFileStatus();
                }
            }, error => {
                this.loading = false;
            }
        );
    }

    ngOnInit(): void {
        this.fileUploadService.checkCurrentAiracStatus().subscribe((airacStatusDto: AiracStatusDto) => {
            this.activeAiracId = airacStatusDto?.airac_id;
            this.activeStatus = airacStatusDto?.status;
            this.activeFileName = airacStatusDto?.file_name;
            this.currentCycle = airacStatusDto?.cycle || undefined;

            if (FileUploadService.STATUSES_TO_RECHECK.indexOf(this.activeStatus) !== -1) {
                this.recheckFileStatus()
            }
        })
    }
}
