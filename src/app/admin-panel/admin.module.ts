import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminRoutes} from "./admin.routes";
import {RouterModule} from "@angular/router";
import {AirportsComponent} from "./view/airports/airports.component";
import {PilotsComponent} from "./view/pilots/pilots.component";
import {DemoMaterialModule} from "../demo-material-module";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {CdkTableModule} from "@angular/cdk/table";
import {TableGridComponent} from "./components/table-grid/table-grid.component";
import { ConfirmPilotChangeDialogComponent } from './view/pilots/confirm-dialog/confirm-pilot-change-dialog/confirm-pilot-change-dialog.component';
import { RoutesComponent } from './view/routes/routes.component';
import {AircraftListComponent} from "./view/aircrafts/aircrafts-list/aircraft-list.component";
import {AircraftTypesComponent} from "./view/aircrafts/aircrafts-types/aircraft-types.component";
import { AircraftTypeEditComponent } from './view/aircrafts/aircrafts-types/edit/aircraft-type-edit/aircraft-type-edit.component';
import {ReactiveFormComponent} from "./components/reactive-form/reactive-form.component";
import {
    ReactiveFormInputComponent
} from "./components/reactive-form/inputs/reactive-form-input/reactive-form-input.component";
import { ReactiveFormFieldComponent } from './components/reactive-form/reactive-form-field/reactive-form-field.component';
import { ReactiveFormSelectComponent } from './components/reactive-form/inputs/reactive-form-select/reactive-form-select.component';
import { ReactiveFormCheckboxComponent } from './components/reactive-form/inputs/reactive-form-checkbox/reactive-form-checkbox.component';
import { ReactiveFormCheckboxGroupComponent } from "./components/reactive-form/inputs/reactive-form-checkbox-group/reactive-form-checkbox-group.component";
import {
    ReactiveFormImageComponent
} from "./components/reactive-form/inputs/reactive-form-image/reactive-form-image.component";
import {
    ReactiveFormFileComponent
} from "./components/reactive-form/inputs/reactive-form-file/reactive-form-file.component";
import { AirportEditComponent } from './view/airports/airport-edit/airport-edit.component';
import { AircraftEditComponent } from './view/aircrafts/aircrafts-list/aircraft-edit/aircraft-edit.component';
import { RouteEditComponent } from './view/routes/edit/route-edit/route-edit.component';
import { ReactiveFormCalendarComponent } from './components/reactive-form/inputs/reactive-form-calendar/reactive-form-calendar.component';
import {MAT_DATE_LOCALE} from "@angular/material/core";
import { QuestionBankListComponent } from './view/exams/question-bank/question-bank-list/question-bank-list.component';
import { QuestionBankEditComponent } from './view/exams/question-bank/question-bank-edit/question-bank-edit.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminRoutes),
        DemoMaterialModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        CdkTableModule
    ],
    providers: [
        {
            provide: MAT_DATE_LOCALE,
            useValue: 'en-US'
        },
    ],
    declarations: [
        TableGridComponent,
        AircraftListComponent,
        AircraftTypesComponent,
        AirportsComponent,
        PilotsComponent,
        ConfirmPilotChangeDialogComponent,
        RoutesComponent,
        AircraftTypeEditComponent,
        ReactiveFormComponent,
        ReactiveFormInputComponent,
        ReactiveFormFieldComponent,
        ReactiveFormSelectComponent,
        ReactiveFormCheckboxComponent,
        ReactiveFormCheckboxGroupComponent,
        ReactiveFormImageComponent,
        ReactiveFormFileComponent,
        AirportEditComponent,
        AircraftEditComponent,
        RouteEditComponent,
        ReactiveFormCalendarComponent,
        QuestionBankListComponent,
        QuestionBankEditComponent
    ]
})
export class AdminModule {
}
