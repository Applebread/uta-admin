import {Routes} from '@angular/router';
import {PilotsComponent} from "./view/pilots/pilots.component";
import {AirportsComponent} from "./view/airports/airports.component";
import {RoutesComponent} from "./view/routes/routes.component";
import {AircraftTypesComponent} from "./view/aircrafts/aircrafts-types/aircraft-types.component";
import {AircraftListComponent} from "./view/aircrafts/aircrafts-list/aircraft-list.component";
import {AircraftTypeEditComponent} from "./view/aircrafts/aircrafts-types/edit/aircraft-type-edit/aircraft-type-edit.component";
import {AirportEditComponent} from "./view/airports/airport-edit/airport-edit.component";
import {AircraftEditComponent} from "./view/aircrafts/aircrafts-list/aircraft-edit/aircraft-edit.component";
import {RouteEditComponent} from "./view/routes/edit/route-edit/route-edit.component";
import {QuestionBankListComponent} from "./view/exams/question-bank/question-bank-list/question-bank-list.component";
import {QuestionBankEditComponent} from "./view/exams/question-bank/question-bank-edit/question-bank-edit.component";

export const AdminRoutes: Routes = [
    {
        path: 'pilots',
        component: PilotsComponent
    },
    {
        path: 'airports',
        component: AirportsComponent
    },
    {
        path: 'airports/:id',
        component: AirportEditComponent
    },
    {
        path: 'airports/new',
        component: AirportEditComponent
    },
    {
        path: 'aircraft-types',
        component: AircraftTypesComponent
    },
    {
        path: 'aircraft-types/:id',
        component: AircraftTypeEditComponent
    },
    {
        path: 'aircraft-types/new',
        component: AircraftTypeEditComponent
    },
    {
        path: 'aircraft-list',
        component: AircraftListComponent
    },
    {
        path: 'aircraft-list/new',
        component: AircraftEditComponent
    },
    {
        path: 'aircraft-list/:id',
        component: AircraftEditComponent
    },
    {
        path: 'routes',
        component: RoutesComponent
    },
    {
        path: 'routes/:id',
        component: RouteEditComponent
    },
    {
        path: 'routes/new',
        component: RouteEditComponent
    },
    {
        path: 'bank-questions',
        component: QuestionBankListComponent
    },
    {
        path: 'bank-questions/:type',
        component: QuestionBankListComponent
    },
    {
        path: 'bank-questions/:type/:acType',
        component: QuestionBankListComponent
    },
    {
        path: 'bank-question/edit/:id',
        component: QuestionBankEditComponent
    },
    {
        path: 'bank-question/new',
        component: QuestionBankEditComponent
    }
];
