import {Component, Inject, OnInit} from '@angular/core';
import {IMinimum, IPilot, IPilotsList, IRank, PilotsService} from "../../../service/pilots/pilots.service";
import {ITableGridParams} from "../../components/table-grid/table-grid.component";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmPilotChangeDialogComponent} from "./confirm-dialog/confirm-pilot-change-dialog/confirm-pilot-change-dialog.component";
import {APP_CONFIG, AppConfig} from "../../../app-config/app-config.module";

@Component({
    selector: 'app-pilots',
    templateUrl: './pilots.component.html',
    styleUrls: ['./pilots.component.scss']
})
export class PilotsComponent implements OnInit{
    public dataSource: Array<IPilot> = [];
    public totalResults: number = 0;
    public displayedColumns: Array<string> = ["id", "avatar", "email", "pid", "name", "flightTime", "minimum", "rank"];
    public rankList: Array<IRank> = [];
    public minimumsList: Array<IMinimum> = [];
    constructor(private pilotsService: PilotsService, public dialog: MatDialog, @Inject(APP_CONFIG) private config: AppConfig) {}

    public handlePageEvent(tableGridParams: ITableGridParams): void {
        this.pilotsService.pilotsList(tableGridParams.pageIndex + 1, <number>tableGridParams.pageSize).subscribe((data: IPilotsList) => {
            this.dataSource = data.items;
            this.totalResults = data.totalResults;
        })
    }

    public changePilotRank(pilot: IPilot, rank: IRank): void {
        const dialogRef = this.dialog.open(ConfirmPilotChangeDialogComponent, {
            data: {
                property: 'rank',
                newValue: rank.name
            },
        });

        dialogRef.afterClosed().subscribe(result => {
            if (!result) {
                return;
            }

            this.pilotsService.updatePilotRank(pilot, rank).subscribe();
        });
    }

    public changeMinimum(pilot: IPilot, minimum: IMinimum): void
    {
        const dialogRef = this.dialog.open(ConfirmPilotChangeDialogComponent,
            {
                data: {
                    property: 'minimum',
                    newValue: minimum.value
                },
            });

        dialogRef.afterClosed().subscribe(result => {
            if (!result) {
                return;
            }

            this.pilotsService.updatePilotMinimum(pilot, minimum).subscribe();
        });
    }

    public getRankIcon(rank: IRank): string
    {
        return this.config.imagesBasePath + rank.icon;
    }

    ngOnInit(): void {
        this.pilotsService.ranksList().subscribe((rankList: Array<IRank>) => {
            this.rankList = rankList;
        });

        this.pilotsService.minimumsList().subscribe((minimumsList: Array<IMinimum>) => {
            this.minimumsList = minimumsList;
        });
    }
}

