import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

interface DialogData {
    property: string
    newValue: string
}

@Component({
    selector: 'app-confirm-pilot-change-dialog',
    templateUrl: './confirm-pilot-change-dialog.component.html',
    styleUrls: ['./confirm-pilot-change-dialog.component.css']
})
export class ConfirmPilotChangeDialogComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }

    ngOnInit(): void {
    }

}
