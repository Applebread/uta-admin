import {Component, Inject} from '@angular/core';
import {ITableGridParams} from "../../../components/table-grid/table-grid.component";
import {AircraftService, IAircraftType, IAircraftTypes} from "../../../../service/aircrafts/aircraft.serves";
import {APP_CONFIG, AppConfig} from "../../../../app-config/app-config.module";

@Component({
    selector: 'app-aircraft',
    templateUrl: './aircraft-types.component.html',
    styleUrls: ['./aircraft-types.component.scss']
})
export class AircraftTypesComponent {
    public dataSource?: Array<IAircraftType>;
    public displayedColumns?: Array<string>;
    public totalResults: number = 0;

    constructor(private aircraftsService: AircraftService, @Inject(APP_CONFIG) private config: AppConfig) {
        this.displayedColumns = ['icao', 'name', "description", "image", "heavy"];
    }

    getImagePath(fileName: string) {
        return `${this.config.imagesBasePath}/${fileName}`
    }

    handlePageEvent(tableGridParams: ITableGridParams): void {
        this.aircraftsService.aircraftTypes(tableGridParams.pageIndex + 1, <number>tableGridParams.pageSize).subscribe((data: IAircraftTypes) => {
            this.dataSource = data.items;
            this.totalResults = data.totalResults;
        })
    }
}
