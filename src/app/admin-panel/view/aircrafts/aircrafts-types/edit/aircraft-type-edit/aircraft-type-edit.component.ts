import {Component, Inject, OnInit} from '@angular/core';
import {AircraftImageUploadStatus, AircraftService, IAircraftType} from "../../../../../../service/aircrafts/aircraft.serves";
import {ActivatedRoute, Router} from "@angular/router";
import {IFormSettingsList} from "../../../../../components/reactive-form/interface/form-settings-list";
import {MatSnackBar} from "@angular/material/snack-bar";
import {APP_CONFIG, AppConfig} from "../../../../../../app-config/app-config.module";
import {FormControl, Validators} from "@angular/forms";
import {IValidatorList} from "../../../../../components/reactive-form/interface/validator-list";

@Component({
    selector: 'app-aircraft-type-edit',
    templateUrl: './aircraft-type-edit.component.html',
    styleUrls: ['./aircraft-type-edit.component.css']
})
export class AircraftTypeEditComponent implements OnInit {
    public id?: Number;
    public aircraftType?: IAircraftType;

    public formSettings?: IFormSettingsList

    public aircraftTypeValidators?: IValidatorList

    constructor(
        private aircraftService: AircraftService,
        private route: ActivatedRoute,
        private _snackBar: MatSnackBar,
        private router: Router,
        @Inject(APP_CONFIG) private config: AppConfig
    ) {
        this.initializeFormValidators();
        this.initializeFormSettings();
    }

    private initializeFormValidators(): void {
        this.aircraftTypeValidators = {
            icao: [Validators.required, Validators.maxLength(4)],
            name: [Validators.required],
            description: [Validators.required],
            image: [Validators.required],
            heavy: []
        };
    }

    private initializeFormSettings(): void {
        this.formSettings = {
            icao: {
                type: 'text',
                label: 'ICAO'
            },
            name: {type: 'text', label: 'Name'},
            description: {type: 'text', label: 'Description'},
            image: {
                type: 'image',
                onUpload: (file: File, formControl: FormControl) => {
                    this.aircraftService.uploadAircraftTypeImage(file).subscribe((response: AircraftImageUploadStatus) => {
                        if (!this.aircraftType) {
                            return;
                        }

                        this.aircraftType.image = response.filename;
                        formControl.setValue(response.filename);
                    });
                },
                label: 'Image',
                acceptedTypes: 'image/png,image/jpg,image/jpeg'
            },
            heavy: {type: 'checkbox', label: 'is heavy'}
        };
    }

    ngOnInit(): void {
        this.updateAircraftType();
    }

    public updateAircraftType(): void
    {
        const typeId = this.route.snapshot.paramMap.get('id');
        this.id = typeId ? parseInt(typeId) : undefined;

        if (!this.id) {
            this.aircraftType = {
                icao: '',
                name: '',
                description: '',
                image: '',
                heavy: false,
            };

            return;
        }

        this.aircraftService.aircraftType(this.id).subscribe((aircraftType: IAircraftType) => {
            this.aircraftType = aircraftType;
        })
    }

    updateType(newValue: IAircraftType): void {
        if (!this.id) {
            this.aircraftService.createAircraftType(newValue).subscribe((type: IAircraftType) => {
                this.aircraftType = type;
                this._snackBar.open("Success");
                this.router.navigate(['/', 'views', 'aircraft-types']);
                setTimeout(() => {
                    this._snackBar.dismiss();
                }, 2000)
            });

            return;
        }

        this.aircraftService.updateAircraftType(this.id, newValue).subscribe((aircraftType: IAircraftType) => {
            this.aircraftType = aircraftType;
            this._snackBar.open("Success");
            this.router.navigate(['/', 'views', 'aircraft-types']);
            setTimeout(() => {
                this._snackBar.dismiss();
            }, 2000)
        });
    }

    back(): void
    {
        this.router.navigate(['/', 'views', 'aircraft-types']);
    }
}
