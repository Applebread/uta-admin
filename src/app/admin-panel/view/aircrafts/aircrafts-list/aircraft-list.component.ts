import {Component} from '@angular/core';
import {ITableGridParams} from "../../../components/table-grid/table-grid.component";
import {AircraftService, IAircraft, IAircraftList} from "../../../../service/aircrafts/aircraft.serves";

@Component({
  selector: 'app-aircraft-list',
  templateUrl: './aircraft-list.component.html',
})
export class AircraftListComponent  {
    public dataSource?: Array<IAircraft>;
    public displayedColumns?: Array<string>;
    public totalResults: number = 0;

  constructor(private aircraftService: AircraftService) {
      this.displayedColumns = ["icao", "location", "gate", "registration", "fob"]
  }

  handlePageEvent(tableGridParams: ITableGridParams): void {
      this.aircraftService.aircraftList(tableGridParams.pageIndex + 1, <number>tableGridParams.pageSize).subscribe((data: IAircraftList) => {
          this.dataSource = data.items;
          this.totalResults = data.totalResults;
      })
  }
}
