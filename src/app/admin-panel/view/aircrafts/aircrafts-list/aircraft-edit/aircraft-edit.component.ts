import {Component, OnInit} from '@angular/core';
import {
    AircraftService,
    IAircraft,
    IAircraftType,
    IAircraftTypes
} from "../../../../../service/aircrafts/aircraft.serves";
import {IFormSettingsList} from "../../../../components/reactive-form/interface/form-settings-list";
import {IValidatorList} from "../../../../components/reactive-form/interface/validator-list";
import {AirportsService, IAirport, IAirportsList} from "../../../../../service/airports/airports.service";
import {firstValueFrom} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Validators} from "@angular/forms";

@Component({
    selector: 'app-aircraft-edit',
    templateUrl: './aircraft-edit.component.html',
    styleUrls: ['./aircraft-edit.component.scss']
})
export class AircraftEditComponent implements OnInit {

    public id?: number;
    public aircraft?: IAircraft;
    public aircraftTypes?: IAircraftTypes;
    public locations?: IAirportsList;
    public formSettings?: IFormSettingsList

    public aircraftValidators?: IValidatorList

    constructor(
        private readonly aircraftService: AircraftService,
        private readonly airportsService: AirportsService,
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly _snackBar: MatSnackBar
    ) {
    }

    async ngOnInit() {
        await this.initializeFormSettings();
        this.initializeFormValidators();
        this.updateAircraft();
    }

    public updateAircraft(): void
    {
        const aircraftId = this.route.snapshot.paramMap.get('id');
        this.id = aircraftId ? parseInt(aircraftId) : undefined;

        if (!this.id) {
            this.aircraft = {
                icao: '',
                location: '',
                gate: '',
                registration: '',
                fob: 0
            };

            return;
        }

        this.aircraftService.aircraft(this.id).subscribe((aircraftType: IAircraft) => {
            this.aircraft = aircraftType;
        })
    }

    updateAircraftState(aircraft: IAircraft): void {
        if (!this.id) {
            this.aircraftService.createAircraft(aircraft).subscribe((type: IAircraft) => {
                this.aircraft = type;
                this._snackBar.open("Success");
                this.router.navigate(['/', 'views', 'aircraft-list']);
                setTimeout(() => {
                    this._snackBar.dismiss();
                }, 2000)
            });

            return;
        }

        this.aircraftService.updateAircraft(this.id, aircraft).subscribe((aircraft: IAircraft) => {
            this.aircraft = aircraft;
            this._snackBar.open("Success");
            this.router.navigate(['/', 'views', 'aircraft-list']);
            setTimeout(() => {
                this._snackBar.dismiss();
            }, 2000)
        });
    }

    back(): void {
        this.router.navigate(['/', 'views', 'aircraft-list']);
    }

    private initializeFormValidators(): void {
        this.aircraftValidators = {
            icao: [Validators.required, Validators.maxLength(4), Validators.minLength(4)],
            location: [Validators.required],
            gate: [Validators.required, Validators.maxLength(5)],
            registration: [Validators.required, Validators.maxLength(8)],
            fob: [Validators.required, Validators.min(0), Validators.max(50000)]
        };
    }

    private async initializeFormSettings() {
        this.aircraftTypes = await firstValueFrom(this.aircraftService.aircraftTypes(1, 500));
        this.locations = await firstValueFrom(this.airportsService.airportList(1, 500));
        this.formSettings = {
            icao: {
                type: 'select',
                label: 'ICAO',
                options: this.aircraftTypes.items.map((type: IAircraftType) => ({
                    label: type.icao,
                    value: type.icao
                }))
            },
            location: {
                type: 'select',
                label: 'Location',
                options: this.locations.items.map((airport: IAirport) => ({
                    label: airport.icao,
                    value: airport.icao
                }))
            },
            gate: {
                type: 'text',
                label: 'Gate'
            },
            registration: {
                type: 'text',
                label: 'Registration'
            },
            fob: {
                type: 'number',
                label: 'Fuel-On-Board (kg)'
            }
        }
    }
}
