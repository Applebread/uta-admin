import {Component, OnInit} from '@angular/core';
import {ITableGridParams} from "../../../../components/table-grid/table-grid.component";
import {
    IQuestionBankItem,
    IQuestionList,
    QuestionBankService
} from "../../../../../service/question-bank/question-bank.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-question-bank-list',
  templateUrl: './question-bank-list.component.html',
  styleUrls: ['./question-bank-list.component.scss']
})
export class QuestionBankListComponent implements OnInit{
    public dataSource?: Array<IQuestionBankItem>;
    public displayedColumns?: Array<string>;
    public totalResults: number = 0;

    constructor(private questionBankService: QuestionBankService, public route: ActivatedRoute) {
        this.displayedColumns = ["id", "question"]
    }

    handlePageEvent(tableGridParams?: ITableGridParams): void {
        const pageIndex = tableGridParams ? tableGridParams.pageIndex + 1 : 1;
        const pageSize = tableGridParams ? tableGridParams.pageSize : 15;
        this.updateList(pageIndex, pageSize);
    }

    private updateList(page: number, pageSize: number): void
    {
        this.questionBankService.questionsList(
            this.route.snapshot.paramMap.get('type') ?? '',
            this.route.snapshot.paramMap.get('acType') ?? '',
            page,
            pageSize
        ).subscribe((data: IQuestionList) => {
            this.dataSource = data.items;
            this.totalResults = data.totalResults;
        })
    }

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            if (params.type_rating !== this.route.snapshot.paramMap.get('acType')) {
                this.handlePageEvent()
            }
        })
    }
}
