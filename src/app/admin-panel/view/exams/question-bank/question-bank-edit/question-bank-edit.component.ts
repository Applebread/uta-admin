import { Component, OnInit } from '@angular/core';
import {
    AircraftService,
    IAircraftType,
    IAircraftTypes
} from "../../../../../service/aircrafts/aircraft.serves";
import {IFormSettingsList} from "../../../../components/reactive-form/interface/form-settings-list";
import {IValidatorList} from "../../../../components/reactive-form/interface/validator-list";
import {
    IQuestionBankItem,
    IQuestionBankItemOption,
    QuestionBankService
} from "../../../../../service/question-bank/question-bank.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {firstValueFrom} from "rxjs";

@Component({
  selector: 'app-question-bank-edit',
  templateUrl: './question-bank-edit.component.html',
  styleUrls: ['./question-bank-edit.component.scss']
})
export class QuestionBankEditComponent implements OnInit {
    public id?: number;
    public question?: IQuestionBankItem;
    public questionOptions?: Array<IQuestionBankItemOption>;
    public aircraftTypes?: IAircraftTypes;
    public questionTypes?: Array<string>;
    public formSettings?: IFormSettingsList
    public questionOptionsFormSettings?: IFormSettingsList
    public questionValidators?: IValidatorList
  constructor(
      private readonly questionBankService: QuestionBankService,
      private readonly aircraftService: AircraftService,
      private readonly route: ActivatedRoute,
      private readonly router: Router,
      private readonly _snackBar: MatSnackBar
  ) { }

    async ngOnInit() {
        await this.initializeFormSettings();
        this.updateQuestion();
    }

    private async initializeFormSettings() {
        this.aircraftTypes = await firstValueFrom(this.aircraftService.aircraftTypes(1, 500));
        this.questionTypes = await firstValueFrom(this.questionBankService.questionTypes())

        this.formSettings = {
            type: {
                type: 'select',
                label: 'Type',
                options: this.questionTypes.map((type: string) => (
                    {
                        label: type,
                        value: type
                    }
                ))
            },
            question: {
                type: 'text',
                label: 'Question'
            }
        }

        this.questionOptionsFormSettings = {
            text: {
                type: 'text',
                label: 'Question Text'
            },
            isCorrect: {
                type: 'checkbox',
                label: 'Is Correct'
            }
        }
    }

    public updateQuestion(): void
    {
        const questionId = this.route.snapshot.paramMap.get('id');
        this.id = questionId ? parseInt(questionId) : undefined;

        if (!this.id) {
            this.question = {
                type: '',
                question: '',
                options: []
            };

            return;
        }

        this.questionBankService.question(this.id).subscribe((question: IQuestionBankItem) => {
            this.question = question;
            this.questionOptions = question.options;

            if (this.formSettings && this.question?.type === 'type_rating') {
                this.formSettings['aircraftType'] = {
                    type: 'select',
                    label: 'Aircraft Type',
                    options: this.aircraftTypes?.items.map((type: IAircraftType) => (
                        {
                            label: type.icao,
                            value: type.icao
                        }
                    ))
                }
            }
        })
    }

    updateQuestionValue(newValue?: any) {
        this.question = newValue;
        // @ts-ignore
        this.question.options = this.questionOptions ?? [];

        console.log(this.question)
    }

    updateQuestionOptionValue(index: number, newValue: IQuestionBankItemOption) {
        // @ts-ignore
        this.question?.options[index] = newValue;

        console.log(this.question)
    }

    updateQuestionState(): void {
        if (!this.question) {
            return;
        }

        if (!this.id) {
            this.questionBankService.createQuestion(this.question).subscribe((question: IQuestionBankItem) => {
                this.question = question;
                this._snackBar.open("Success");
                this.router.navigate(['/', 'views', 'bank-questions']);
                setTimeout(() => {
                    this._snackBar.dismiss();
                }, 2000)
            });

            return;
        }

        this.questionBankService.updateQuestion(this.id, this.question).subscribe((question: IQuestionBankItem) => {
            this.question = question;
            this._snackBar.open("Success");
            this.router.navigate(['/', 'views', 'bank-questions']);
            setTimeout(() => {
                this._snackBar.dismiss();
            }, 2000)
        });
    }

    back(): void {
        this.router.navigate(['/', 'views', 'bank-questions']);
    }
}
