import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {IFormSettingsList} from "../../../../components/reactive-form/interface/form-settings-list";
import {IValidatorList} from "../../../../components/reactive-form/interface/validator-list";
import {IRoute, RoutesService} from "../../../../../service/routes/routes.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AircraftService, IAircraftType} from "../../../../../service/aircrafts/aircraft.serves";
import {AirportsService, IAirport} from "../../../../../service/airports/airports.service";
import {firstValueFrom} from "rxjs";

@Component({
    selector: 'app-route-edit',
    templateUrl: './route-edit.component.html',
    styleUrls: ['./route-edit.component.css']
})
export class RouteEditComponent implements OnInit {
    public id: number | null;
    public route?: IRoute;
    public formSettings?: IFormSettingsList
    public routeValidators?: IValidatorList;

    constructor(
        private routesService: RoutesService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private aircraftService: AircraftService,
        private airportsService: AirportsService,
        private _snackBar: MatSnackBar
    ) {
        const id = activatedRoute.snapshot.paramMap.get('id');
        this.id = id ? parseInt(id) : null;

        this.initForm()
        this.initValidators()
    }

    private async initForm(): Promise<void> {
        const aircraftTypes = await firstValueFrom(this.aircraftService.aircraftTypes(1, 500));
        const locations = await firstValueFrom(this.airportsService.airportList(1, 500));

        this.formSettings = {
            flightNumber: {
                type: 'text',
                label: 'flight number'
            },
            from: {
                type: 'select',
                label: 'From (ICAO)',
                options: locations.items.map((airport: IAirport) => ({
                    label: airport.icao,
                    value: airport.icao
                }))
            },
            to: {
                type: 'select',
                label: 'To (ICAO)',
                options: locations.items.map((airport: IAirport) => ({
                    label: airport.icao,
                    value: airport.icao
                }))
            },
            daysOfWeek: {
                type: 'checkbox-group',
                label: 'Days of week',
                options: [
                    {
                        label: 'Monday',
                        value: '1'
                    },
                    {
                        label: 'Tuesday',
                        value: '2'
                    },
                    {
                        label: 'Wednesday',
                        value: '3'
                    },
                    {
                        label: 'Thursday',
                        value: '4'
                    },
                    {
                        label: 'Friday',
                        value: '5'
                    },
                    {
                        label: 'Saturday',
                        value: '6'
                    },
                    {
                        label: 'Sunday',
                        value: '7'
                    }
                ]
            },
            departureTime: {
                type: 'text',
                label: 'Departure time'
            },
            scheduledSince: {
                type: 'calendar',
                label: 'Scheduled since'
            },
            scheduledTill: {
                type: 'calendar',
                label: 'Scheduled till'
            },
            aircraftType: {
                type: 'select',
                label: 'Aircraft type',
                options: aircraftTypes.items.map((type: IAircraftType) => ({
                    label: type.icao,
                    value: type.icao
                }))
            },
            alternates: {
                type: 'text',
                label: 'alternates'
            },
        };
    }

    async updateRoute(route: IRoute): Promise<void> {
        this.route = route;
    }

    async back(): Promise<void> {
        await this.router.navigate(['/', 'views', 'routes'])
    }

    private initValidators(): void {
        this.routeValidators = {
            flightNumber: [],
            from: [],
            to: [],
            daysOfWeek: [],
            departureTime: [],
            scheduledSince: [],
            scheduledTill: [],
            aircraftType: [],
            alternates: []
        };
    }

    async ngOnInit(): Promise<void> {
        if (!this.id) {
            const today = new Date();
            const todayString = `${today.getFullYear()}.${today.getMonth()}.${today.getDate()}`;

            this.route = {
                id: 0,
                flightNumber: '',
                from: '',
                to: '',
                daysOfWeek: [],
                departureTime: '',
                scheduledSince: todayString,
                scheduledTill: todayString,
                aircraftType: '',
                alternates: []
            };

            return;
        }

        this.routesService.route(this.id).subscribe((route: IRoute) => {
            this.route = route;
        });
    }

}
