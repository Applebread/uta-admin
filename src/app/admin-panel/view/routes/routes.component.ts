import {Component, OnInit} from '@angular/core';
import {ITableGridParams} from "../../components/table-grid/table-grid.component";
import {IRoute, IRouteList, RoutesService} from "../../../service/routes/routes.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {AirportsService, IAirport, IAirportsList} from "../../../service/airports/airports.service";
import {map, Observable, startWith} from "rxjs";
import {AircraftService, IAircraftType, IAircraftTypes} from "../../../service/aircrafts/aircraft.serves";

@Component({
    selector: 'app-routes',
    templateUrl: './routes.component.html',
    styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {

    dataSource?: Array<IRoute> = [];

    totalResults: Number = 0;

    filterForm: FormGroup;

    airports?: IAirportsList

    loading: boolean = false

    availableAircraftTypes: Array<string> = [];

    public displayedColumns?: Array<string>;
    public filteredFromAirports?: Observable<IAirport[]>;
    public filteredToAirports?: Observable<IAirport[]>;

    constructor(
        private airportService: AirportsService,
        private routesService: RoutesService,
        private formBuilder: FormBuilder,
        private aircraftService: AircraftService
    ) {
        this.displayedColumns = ["id", "flightNumber", "from", "to", "aircraftType", "etd", "captain", "fo", "status"];
        this.filterForm = this.formBuilder.group({
            aircraftType: '',
            dateOfFlight: '',
            from: '',
            to: '',
        });

        airportService.airportList(1, 500).subscribe((airports: IAirportsList) => {
            this.airports = airports
        });

        aircraftService.aircraftTypes(1, 500).subscribe((aircraftTypes: IAircraftTypes) => {
            this.availableAircraftTypes = aircraftTypes.items.map((type: IAircraftType) => type.icao);
        })
    }

    ngOnInit(): void {
        this.filteredFromAirports = this.filterForm.get('from')?.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value || '')),
        );

        this.filteredToAirports = this.filterForm.get('to')?.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value || '')),
        );
    }

    private _filter(value: string): IAirport[] {
        if (!this.airports) {
            return [];
        }

        const filterValue = value.toLowerCase();

        return this.airports.items.filter((option: IAirport) => `${option.icao} ${option.name}`.toLowerCase().includes(filterValue));
    }

    public submitFilter(): void
    {
        this.updateData({
            length: 15,
            pageIndex: 0,
            pageSize: 15,
            searchValue: ''
        });
    }

    private updateData(tableGridParams: ITableGridParams): void
    {
        this.routesService.routesList(tableGridParams.pageIndex + 1, <number>tableGridParams.pageSize, this.filterForm.value).subscribe((data: IRouteList) => {
            this.dataSource = data.items;
            this.totalResults = data.totalResults;
        })
    }

    handlePageEvent(tableGridParams: ITableGridParams): void {
        this.updateData(tableGridParams);
    }
}
