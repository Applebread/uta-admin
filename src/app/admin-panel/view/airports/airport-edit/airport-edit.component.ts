import {Component, OnInit} from '@angular/core';
import {AirportsService, IAirport} from "../../../../service/airports/airports.service";
import {ActivatedRoute, Router} from "@angular/router";
import {IFormSettingsList} from "../../../components/reactive-form/interface/form-settings-list";
import {IValidatorList} from "../../../components/reactive-form/interface/validator-list";
import {Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-airport-edit',
    templateUrl: './airport-edit.component.html',
    styleUrls: ['./airport-edit.component.scss']
})
export class AirportEditComponent implements OnInit {
    public id?: number;
    public airport?: IAirport;
    public formSettings?: IFormSettingsList
    public airportValidators?: IValidatorList;

    constructor(private airportService: AirportsService, private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar) {
        this.initForm();
        this.initFormValidators();
    }

    private initForm(): void {
        this.formSettings = {
            icao: {
                type: 'text',
                label: 'ICAO'
            },
            name: {
                type: 'text',
                label: 'Name'
            },
            longitude: {
                type: 'text',
                label: 'Longitude'
            },
            latitude: {
                type: 'text',
                label: 'Latitude'
            },
            city: {
                type: 'text',
                label: 'City'
            },
            timezone: {
                type: 'number',
                label: 'Timezone'
            },
        }
    }

    private initFormValidators(): void {
        this.airportValidators = {
            icao: [Validators.required, Validators.maxLength(4), Validators.minLength(4)],
            name: [Validators.required],
            longitude: [Validators.required],
            latitude: [Validators.required],
            city: [Validators.required],
            timezone: [Validators.required, Validators.max(14), Validators.min(-12)],
        };
    }

    ngOnInit(): void {
        const airportId = this.route.snapshot.paramMap.get('id');
        this.id = airportId ? parseInt(airportId) : undefined;

        if (!this.id) {
            this.airport = {
                icao: '',
                name: '',
                longitude: '',
                latitude: '',
                city: '',
                source: 'manual',
                timezone: ''
            };

            return;
        }

        this.airportService.airport(this.id).subscribe((airport: IAirport) => {
            this.airport = airport;
        });
    }

    back() {
        this.router.navigate(['/', 'views', 'airports']);
    }

    updateAirport($event: IAirport): void {
        if (!this.id) {
            this.airportService.createAirport($event).subscribe((airport: IAirport) => {
                this.airport = airport;
                this._snackBar.open("Success");
                this.router.navigate(['/', 'views', 'airports']);
                setTimeout(() => {
                    this._snackBar.dismiss();
                }, 2000)
            });

            return;
        }

        this.airportService.updateAirport(this.id, $event).subscribe((airport: IAirport) => {
            this.airport = airport;
            this._snackBar.open("Success");
            this.router.navigate(['/', 'views', 'airports']);
            setTimeout(() => {
                this._snackBar.dismiss();
            }, 2000)
        })
    }
}
