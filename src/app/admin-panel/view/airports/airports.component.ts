import {Component} from '@angular/core';
import {AirportsService, IAirport, IAirportsList} from "../../../service/airports/airports.service";
import {ITableGridParams} from "../../components/table-grid/table-grid.component";

@Component({
    selector: 'app-airports',
    templateUrl: './airports.component.html',
    styleUrls: ['./airports.component.scss']
})
export class AirportsComponent {
    public dataSource?: Array<IAirport>;
    public displayedColumns?: Array<string>;
    public totalResults: number = 0;

    constructor(private airportsService: AirportsService) {
        this.displayedColumns = ['icao', 'city', 'name', 'latitude', 'longitude', 'source'];
    }

    handlePageEvent(tableGridParams: ITableGridParams): void {
        this.airportsService.airportList(tableGridParams.pageIndex + 1, <number>tableGridParams.pageSize).subscribe((data: IAirportsList) => {
            this.dataSource = data.items;
            this.totalResults = data.totalResults;
        })
    }
}
