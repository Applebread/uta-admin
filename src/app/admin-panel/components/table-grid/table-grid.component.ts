import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {SelectionModel} from '@angular/cdk/collections';

export interface ITableGridParams {
    length: number;
    pageIndex: number;
    pageSize: number;
    searchValue: string;
}

@Component({
    selector: 'app-table-grid',
    templateUrl: './table-grid.component.html',
    styleUrls: ['./table-grid.component.scss']
})
export class TableGridComponent implements OnInit {
    @Input() public dataSource?: Array<Object>;
    @Input() public displayedColumns?: Array<string>;
    @Input() public length: number = 0;
    @Input() itemTemplate?: TemplateRef<any>;

    public pageSize: number = 15;
    public pageIndex: number = 0;
    public searchValue: string = '';

    @Output() paramsChanged = new EventEmitter<ITableGridParams>();

    constructor(private route: ActivatedRoute, private router: Router) {
    }

    handlePageEvent(event: ITableGridParams): void {
        const queryParams: Params = {
            page: event.pageIndex,
            page_size: event.pageSize
        };

        this.router.navigate(
            [],
            {
                relativeTo: this.route,
                queryParams: queryParams,
                queryParamsHandling: 'merge'
            }).then(r => {});
    }

    ngOnInit(): void {
        this.route.queryParams.subscribe((params: Params) => {
            const page: number = params.page ? +params.page : 0;
            const pageSize: number = params.page_size ? +params.page_size : 15;

            this.pageIndex = page;
            this.pageSize = pageSize;

            this.paramsChanged.emit({
                length: this.length,
                pageIndex: page,
                pageSize: pageSize,
                searchValue: this.searchValue
            });
        })
    }

    selection = new SelectionModel<any>(true, []);

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource?.length;
        return numSelected === numRows;
    }

    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        if (!this.dataSource) {
            return;
        }

        this.selection.select(...this.dataSource);
    }

    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    }
}
