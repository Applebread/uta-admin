import {IFormSettings} from "./form-settings";

export interface IFormSettingsList {
    [key: string]: IFormSettings
}
