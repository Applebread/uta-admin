import {IFormSelectOption} from "./select-option";
import {FormControl} from "@angular/forms";

export interface IFormSettings {
    type: 'text' | 'checkbox' | 'select' | 'number' | 'image' | 'checkbox-group' | 'calendar'
    options?: Array<IFormSelectOption>;
    label: string
    placeholder?: string
    onUpload?: (file: File, formControl: FormControl) => void;
    acceptedTypes?: string;
}
