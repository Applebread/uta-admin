import {FormControl, ValidationErrors} from "@angular/forms";

export interface IValidatorList {
    [key: string]: Array<(formControl: FormControl) => ValidationErrors | null>
}
