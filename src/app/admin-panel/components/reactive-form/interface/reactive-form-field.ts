import {IFormSettings} from "./form-settings";
import {FormControl, ValidatorFn} from "@angular/forms";

export interface ReactiveFormField {
    data?: string | number | Array<string> | Array<number>;

    setting?: IFormSettings;

    fieldName: string

    control?: FormControl

    validators?: Array<ValidatorFn>;
}
