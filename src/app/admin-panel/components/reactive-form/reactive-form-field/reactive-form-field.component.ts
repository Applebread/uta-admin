import {Component, EventEmitter, Input, Output} from '@angular/core';
import { IFormSettings } from '../interface/form-settings';
import {ReactiveFormField} from "../interface/reactive-form-field";
import {FormControl, ValidatorFn} from "@angular/forms";

@Component({
    selector: 'app-reactive-form-field',
    templateUrl: './reactive-form-field.component.html',
    styleUrls: ['./reactive-form-field.component.scss']
})
export class ReactiveFormFieldComponent implements ReactiveFormField {

    @Input() public component?: string;

    @Input() public fieldName: string = '';

    @Input() data: string | number | Array<string> | Array<number> = '';

    @Output() changed: EventEmitter<string | number | Array<string> | Array<number>>;

    @Input() control?: FormControl;

    @Input() setting?: IFormSettings;

    @Input() validators: Array<ValidatorFn> = [];

    constructor() {
        this.changed = new EventEmitter();
    }
}
