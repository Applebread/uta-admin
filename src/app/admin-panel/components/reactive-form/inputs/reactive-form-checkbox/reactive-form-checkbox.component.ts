import {Component, Input} from '@angular/core';
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {FormControl, ValidatorFn} from "@angular/forms";
import {IFormSettings} from "../../interface/form-settings";

@Component({
    selector: 'app-reactive-form-checkbox',
    templateUrl: './reactive-form-checkbox.component.html',
    styleUrls: ['./reactive-form-checkbox.component.css']
})
export class ReactiveFormCheckboxComponent implements ReactiveFormField {

    @Input() data: string | number | Array<string> | Array<number> = '';

    @Input() fieldName: string = '';

    @Input() type: string = 'text';

    @Input() control?: FormControl

    @Input() setting?: IFormSettings;

    @Input() validators: Array<ValidatorFn> = [];
}
