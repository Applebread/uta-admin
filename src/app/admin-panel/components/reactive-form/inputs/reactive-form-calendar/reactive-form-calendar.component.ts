import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {ReactiveFormFieldComponent} from "../../reactive-form-field/reactive-form-field.component";
import {FormControl, ValidatorFn} from "@angular/forms";
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {IFormSettings} from "../../interface/form-settings";

@Component({
    selector: 'app-reactive-form-calendar',
    templateUrl: './reactive-form-calendar.component.html',
    styleUrls: ['./reactive-form-calendar.component.scss']
})
export class ReactiveFormCalendarComponent implements ReactiveFormField {
    @Input() data: string | number | Array<string> | Array<number> = '';

    @Input() fieldName: string = '';

    @Input() type: string = 'text';

    @Input() control?: FormControl

    @Input() setting?: IFormSettings;

    @Input() validators: Array<ValidatorFn> = [];

    selected?: string

    constructor() {
    }

    ngOnInit(): void {
    }

    dateChange($event: any): void {
        this.control?.setValue(`${$event.value.getFullYear()}.${$event.value.getMonth()}.${$event.value.getDate()}`)
    }
}
