import {Component, Input, OnInit} from '@angular/core';
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {FormControl, ValidatorFn} from "@angular/forms";
import {IFormSettings} from "../../interface/form-settings";

@Component({
  selector: 'app-reactive-form-file',
  templateUrl: './reactive-form-file.component.html',
  styleUrls: ['./reactive-form-file.component.css']
})
export class ReactiveFormFileComponent implements ReactiveFormField {
    @Input() control?: FormControl;
    @Input() data?: string | number | Array<string> | Array<number>;
    @Input() fieldName: string = '';
    @Input() setting?: IFormSettings;
    @Input() validators: Array<ValidatorFn> = [];
}
