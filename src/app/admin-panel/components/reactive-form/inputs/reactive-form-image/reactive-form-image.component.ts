import {Component, Inject, Input, OnInit} from '@angular/core';
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {FormControl, ValidatorFn} from "@angular/forms";
import {IFormSettings} from "../../interface/form-settings";
import {APP_CONFIG, AppConfig} from "../../../../../app-config/app-config.module";

@Component({
  selector: 'app-reactive-form-image',
  templateUrl: './reactive-form-image.component.html',
  styleUrls: ['./reactive-form-image.component.scss']
})
export class ReactiveFormImageComponent implements ReactiveFormField {
    @Input() public readonly control?: FormControl;
    @Input() public readonly data?: string | number | Array<string> | Array<number>;
    @Input() public readonly fieldName: string = '';
    @Input() public readonly setting?: IFormSettings;
    @Input() public readonly validators: Array<ValidatorFn> = [];
    @Input() public readonly type: string = '';
    @Input() public readonly onUpload?: (file: File, formControl: FormControl) => void
    @Input() public readonly requiredFileType?: string = ''
    public fileName?: string;
    public uploadedImage?: string;

    constructor(@Inject(APP_CONFIG) private config: AppConfig) {
    }

    public imageUrl(): string | null
    {
        if (!this.data) {
            return null;
        }

        return `${this.config.imagesBasePath}/${this.data}`
    }

    fileSelected(file: File) {
        if (!file || !this.control || !this.onUpload) {
            return;
        }

        this.fileName = file.name;
        this.onUpload(file, this.control);
    }
}
