import {Component, Input, OnInit} from '@angular/core';
import {FormControl, ValidatorFn} from "@angular/forms";
import {IFormSettings} from "../../interface/form-settings";
import {ReactiveFormField} from "../../interface/reactive-form-field";

@Component({
  selector: 'app-reactive-form-select',
  templateUrl: './reactive-form-select.component.html',
  styleUrls: ['./reactive-form-select.component.scss']
})
export class ReactiveFormSelectComponent implements ReactiveFormField {
    @Input() data: string | number | Array<string> | Array<number> = '';
    @Input() fieldName: string = '';
    @Input() type: string = 'text';
    @Input() control?: FormControl
    @Input() setting?: IFormSettings;
    @Input() validators: Array<ValidatorFn> = [];
}
