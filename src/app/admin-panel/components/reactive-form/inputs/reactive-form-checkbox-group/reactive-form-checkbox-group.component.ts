import {Component, Input, OnInit} from '@angular/core';
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {FormControl, ValidatorFn} from "@angular/forms";
import {IFormSettings} from "../../interface/form-settings";
import {IFormSelectOption} from "../../interface/select-option";

@Component({
    selector: 'app-reactive-form-checkbox-group',
    templateUrl: './reactive-form-checkbox-group.component.html',
    styleUrls: ['./reactive-form-checkbox-group.component.css']
})
export class ReactiveFormCheckboxGroupComponent implements ReactiveFormField, OnInit {

    @Input() data: string | number | Array<string> | Array<number> = '';

    @Input() fieldName: string = '';

    @Input() type: string = 'text';

    @Input() control?: FormControl

    @Input() setting?: IFormSettings;

    @Input() validators: Array<ValidatorFn> = [];

    value?: Array<boolean> | Object

    constructor() {
    }

    ngOnInit(): void {
        if (!this.setting?.options) {
            this.value = [];

            return;
        }

        const value = {};

        this.setting?.options.forEach((option: IFormSelectOption) => {
            Object.assign(value, {[option.value]: false});
        })

        this.value = value;
    }

    updateValue(): void {
        this.control?.setValue(this.value);
    }
}
