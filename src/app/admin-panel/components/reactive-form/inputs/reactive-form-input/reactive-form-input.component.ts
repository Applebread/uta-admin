import {Component, Input} from '@angular/core';
import {ReactiveFormField} from "../../interface/reactive-form-field";
import {IFormSettings} from "../../interface/form-settings";
import {FormControl, ValidatorFn} from "@angular/forms";

@Component({
    selector: 'app-reactive-form-input',
    templateUrl: './reactive-form-input.component.html',
    styleUrls: ['./reactive-form-input.component.css']
})
export class ReactiveFormInputComponent implements ReactiveFormField {
    @Input() data: string | number | Array<string> | Array<number> = '';
    @Input() fieldName: string = '';
    @Input() type: string = 'text';
    @Input() control?: FormControl
    @Input() setting?: IFormSettings;
    @Input() validators: Array<ValidatorFn> = [];
}
