import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormControlStatus, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {IFormSettingsList} from "./interface/form-settings-list";

@Component({
    selector: 'app-reactive-form',
    templateUrl: './reactive-form.component.html',
    styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {

    @Input() public dataSource: any;

    @Input() public readonly validators: {[key: string]: Array<ValidatorFn>} = {};

    @Input() public readonly settings: IFormSettingsList = {};

    @Output() public readonly changed = new EventEmitter<Object>();

    @Output() public readonly submitted = new EventEmitter<Object>();

    @Output() public readonly cancelled = new EventEmitter();

    public formGroup?: FormGroup

    public formControls: {[key: string]: FormControl} = {};

    public formHasErrors: boolean = false;

    constructor() {
    }

    public doSubmit(): void {
        this.submitted.emit(this.formGroup?.value)
    }

    public doCancel(): void {
        this.cancelled.emit()
    }

    ngOnInit(): void {
        Object.keys(this.settings).forEach((key: string) => {
            const control = new FormControl(this.dataSource[key], this.validators[key]);

            Object.assign(this.formControls, {[key]: control})
        })

        this.formGroup = new FormGroup(this.formControls);
        this.formGroup.valueChanges.subscribe((value: Object) => {
            this.dataSource = value;
            this.changed.emit(value);
        })

        this.formGroup.statusChanges.subscribe((status: FormControlStatus) => {
            this.formHasErrors = status === 'INVALID';
        })

        this.formHasErrors = this.formGroup.status === 'INVALID';
    }
}
