import {Injectable} from '@angular/core';
import {IQuestionExistentType, QuestionBankService} from "../../service/question-bank/question-bank.service";

export interface Menu {
    state?: string;
    name: string;
    icon: string;
    children?: Array<Menu>
    badge?: Array<IBadge>
}

export interface IBadge {
    type: string;
    value: string
}

const MENUITEMS = [
    {state: 'dashboard', name: 'Dashboard', icon: 'av_timer'},
    {state: 'views/pilots', name: 'Pilots', icon: 'groups'},
    {state: 'views/airports', name: 'Airports', icon: 'flight_takeoff'},
    {
        name: 'Aircrafts', icon: 'flight', children: [
            {state: 'views/aircraft-types', name: 'Aircrafts Types', icon: ''},
            {state: 'views/aircraft-list', name: 'All Aircrafts', icon: ''}
        ]
    },
    { state: 'views/routes', type: 'link', name: 'Routes', icon: 'route' }
];

@Injectable()
export class MenuItems {
    constructor(private questionBankService: QuestionBankService) {
        const questionTypes = this.questionBankService.questionExistentTypes();

        questionTypes.subscribe((data: Array<IQuestionExistentType>) => {
            MENUITEMS.push({
                name: 'Question Bank', icon: 'device_unknown', children: data.map((item: IQuestionExistentType) => {
                    return {
                        state: `views/bank-questions/${item.type}${item.aircraftType ? '/' + item.aircraftType : ''}`,
                        name: `${item.type}${item.aircraftType ? ': ' + item.aircraftType : ''}`,
                        icon: 'format_align_justify'
                    }
                })
            })
        });
    }

    getMenuitem(): Menu[] {
        return MENUITEMS;
    }
}
