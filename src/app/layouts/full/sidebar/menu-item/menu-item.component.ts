import {Component, Input} from '@angular/core';
import {Menu} from "../../../../shared/menu-items/menu-items";

@Component({
    selector: 'app-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent {

    @Input()
    public menuItem?: Menu;
    public accordionOpened: boolean = false;

    constructor() {
    }

    toggleAccordion(): void {
        this.accordionOpened = !this.accordionOpened
    }
}
