import {Routes} from '@angular/router';

import {FullComponent} from './layouts/full/full.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: FullComponent,
        children: [
            {
                path: '',
                redirectTo: '/dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'views',
                loadChildren: () => import('./admin-panel/admin.module').then(m => m.AdminModule)
            }
        ]
    }
];
