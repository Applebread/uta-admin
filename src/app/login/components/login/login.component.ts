import {Component, OnInit} from '@angular/core';

import {FormGroup, FormControl} from '@angular/forms';
import {AuthService} from "../../../service/auth/auth.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public userName: string;
    public password: string;
    public formData: FormGroup;
    public error?: string

    constructor(public authService: AuthService) {
        this.formData = new FormGroup({
            userName: new FormControl(""),
            password: new FormControl(""),
        });

        this.userName = '';
        this.password = ''
    }

    ngOnInit() {
        this.formData = new FormGroup({
            userName: new FormControl(""),
            password: new FormControl(""),
        });
    }

    async onClickSubmit(data: any) {
        this.userName = data.userName;
        this.password = data.password;

        await this.authService.login(this.userName, this.password);
    }
}
