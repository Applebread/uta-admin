export interface AiracStatusDto {
    airac_id: number;
    file_name: string;
    cycle: string | null;
    status: string;
}
