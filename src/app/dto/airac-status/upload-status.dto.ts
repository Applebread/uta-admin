export interface UploadStatusDto {
    id: number;
    started: boolean;
    error: string;
}
