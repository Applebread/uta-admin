export const environment = {
    production: false,
    apiEndpoint: "http://localhost:8083",
    imagesBasePath: "http://localhost:8083"
};
